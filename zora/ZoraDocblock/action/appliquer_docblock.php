<?php
/**
 * Action appliquant un docblock au code source
 *
 * @plugin     Zora Docblock
 * @copyright  2013
 * @author     Matthieu Marcillaud
 * @licence    GNU/GPL
 * @package    SPIP\Zoradocblock\Actions
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Applique un docblock au code source
 */
function action_appliquer_docblock_dist() {
	$securiser_action = charger_fonction('securiser_action','inc');
	$id_docblock = $securiser_action();

	if ($id_docblock) {
		// retrouver le fichier et les infos actuels du docblock
		$ligne = sql_fetsel(
			'element, fichier, type_element, docblock',
			'spip_docblocks',
			'id_docblock = ' . intval($id_docblock));

		if (!$ligne) {
			return false;
		}

		$nouveau = trim($ligne['docblock']);
		// éviter là aussi les CRLF de windows…
		$nouveau = str_replace("\r\n", "\n", $nouveau);
		unset($ligne['docblock']);

		include_spip('inc/zoracode');
		$ligne['file'] = ZORACODE_SOURCE . $ligne['fichier'];
		$docblock = getDocBlock($ligne);
		if ($docblock['erreur']) {
			spip_log("Erreur pour appliquer un docblock", 'zoradocblock.2');
			spip_log($id_docblock, 'zoradocblock.2');
			spip_log($ligne, 'zoradocblock.2');
			spip_log($docblock, 'zoradocblock.2');
			return false;
		}

		// parfois (absence de docblock de fichiers) on peut demander
		// à ajouter des lignes d'espacement…
		if (isset($docblock['addspace']) and $docblock['addspace']) {
			$nouveau = "\n" . $nouveau . "\n";
		}

		// là on a le début et la fin du docblock actuel dans le fichier
		// on l'applique au fichier
		$contenu = file($ligne['file']);
		array_splice($contenu,
			$docblock['start'] - 1, // commence à 0
			($docblock['end'] - $docblock['start'] + 1),
			array($nouveau . "\n"));
		file_put_contents($ligne['file'], $contenu);

		// inscrire comme présent dans le code source
		include_spip('inc/config');
		$presents = lire_config('zora/docblock/appliques', array());
		$presents[] = $id_docblock;
		$presents = array_unique($presents);
		ecrire_config('zora/docblock/appliques', $presents);
		
		// statut indiquant qu'il est appliqué au code source
		include_spip('action/editer_objet');
		objet_instituer('docblock', $id_docblock, array('statut' => 'applique'));
	}
}
