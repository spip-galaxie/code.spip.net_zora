<?php
/**
 * Action faisant un svn up du code source
 *
 * @plugin     Zora Docblock
 * @copyright  2013
 * @author     Matthieu Marcillaud
 * @licence    GNU/GPL
 * @package    SPIP\Zoradocblock\Actions
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Exécute un svn up du code source.
 */
function action_executer_svnup_dist() {
	$securiser_action = charger_fonction('securiser_action','inc');
	$arg = $securiser_action();

	if ($arg == 'oui') {
		$out = array();
		$svn = defined('ZORACODE_SVN_CMD') ? ZORACODE_SVN_CMD : 'svn';
		exec('cd ' . escapeshellarg(realpath(ZORACODE_SOURCE)) . " && $svn up", $out, $err);
		if ($out or $err) {
			spip_log("\nsvn up\n-------", 'zoradocblock.2');
			if ($out) spip_log($out, 'zoradocblock.2');
			if ($err) spip_log($err, 'zoradocblock.2');
		}
	}
}
