<?php
/**
 * Action revertant les docblocks appliqués du code source
 *
 * @plugin     Zora Docblock
 * @copyright  2013
 * @author     Matthieu Marcillaud
 * @licence    GNU/GPL
 * @package    SPIP\Zoradocblock\Actions
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Enlever les docblocks appliqués du code source
 */
function action_reverter_docblocks_dist() {
	$securiser_action = charger_fonction('securiser_action','inc');
	$arg = $securiser_action();

	if ($arg == 'oui') {
		include_spip('inc/config');
		$presents = lire_config('zora/docblock/appliques', array());
		// svn revert
		include_spip('formulaires/commiter_docblocks');
		list($res, $err) = lancer_commande_svn_partout('revert');
		if ($res or $err) {
			spip_log("\nsvn revert\n-------", 'zoradocblock.2');
			if ($res) spip_log($out, 'zoradocblock.2');
			if ($err) spip_log($err, 'zoradocblock.2');
		}

		effacer_config('zora/docblock/appliques');
		include_spip('action/editer_objet');
		foreach ($presents as $id_docblock) {
			objet_instituer('docblock', $id_docblock, array('statut' => 'prop'));
		}
	}
}
