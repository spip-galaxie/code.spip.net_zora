<?php

/**
 * Définit les pipelines utilisés du plugin Zora Docblock
 *
 * @plugin     Zora Docblock
 * @copyright  2013
 * @author     Matthieu Marcillaud
 * @licence    GNU/GPL
 * @package    SPIP\Zoradocblock\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Optimiser la base de donnee en supprimant les liens orphelins
 *
 * @param array $flux
 * @return array
 */
function zoradocblock_optimiser_base_disparus($flux){
	$n = &$flux['data'];
	$mydate = sql_quote(trim($flux['args']['date'], "'"));

	sql_delete("spip_docblocks", "maj < $mydate AND statut = 'poubelle'");

	return $flux;

}


?>
