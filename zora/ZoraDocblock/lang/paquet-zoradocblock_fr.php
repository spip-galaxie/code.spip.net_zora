<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// Z
	'zoradocblock_description' => '',
	'zoradocblock_nom' => 'Zora Docblock',
	'zoradocblock_slogan' => 'Permet de proposer des docblocks et de les commiter',
);

?>