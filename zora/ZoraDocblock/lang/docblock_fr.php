<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_docblock' => 'Ajouter ce docblock',
	'appliquer_docblock' => 'Appliquer ce docblock au code source',

	// B
	'bouton_appliquer' => 'Appliquer',
	'bouton_reverter' => 'Reverter',
	'bouton_svn_up' => 'SVN up !',

	// D
	'docblocks_appliques' => 'Docblocks appliqués au code source',

	// E
	'explication_auteur' => 'Nom ou pseudo de l\'auteur de la proposition',
	'explication_relecteur' => 'Nom ou pseudo du relecteur des propositions.',
	'explication_parent' => 'Type et nom du parent si nécessaire tel que \'class:Nom\'',
	'explication_type_element' => 'Type tel que \'function\', \'file\', \'constant\', \'property\' ou \'class\' ou \'method\' ...',

	// I
	'icone_creer_docblock' => 'Créer un docblock',
	'icone_modifier_docblock' => 'Modifier ce docblock',
	'info_1_docblock' => 'Un docblock',
	'info_aucun_docblock' => 'Aucun docblock',
	'info_docblocks_auteur' => 'Les docblocks de cet auteur',
	'info_nb_docblocks' => '@nb@ docblocks',

	// L
	'label_auteur' => 'Auteur',
	'label_docblock' => 'Docblock',
	'label_element' => 'Élément',
	'label_fichier' => 'Fichier',
	'label_parent' => 'Parent',
	'label_type_element' => 'Type d\'élément',
	'label_message_commit' => 'Message de commit',
	'label_relecteur' => 'Relecteur',
	'label_resume' => 'Résumé des modifications',

	// N
	'notification_propose_detail' => 'Le docblock «@titre@» est proposée à la publication depuis',
	'notification_propose_sujet' => '[@nom_site_spip@] @auteur@ propose : @titre@ (dans @fichier@)',
	'notification_propose_titre' => "Docblock proposé\n------------",
	'notification_propose_url' => 'Docblock disponible à l\'adresse :',

	// P
	'preparer_envoi' => "Préparer l'envoi",

	// R
	'retirer_lien_docblock' => 'Retirer ce docblock',
	'retirer_tous_liens_docblocks' => 'Retirer tous les docblocks',

	// T
	'texte_ajouter_docblock' => 'Ajouter un docblock',
	'texte_changer_statut_docblock' => 'Ce docblock est :',
	'texte_creer_associer_docblock' => 'Créer et associer un docblock',
	'texte_statut_applique' => 'Appliqué au code source',
	'titre_docblock' => 'Docblock',
	'titre_docblocks' => 'Docblocks',
	'titre_docblocks_commit' => 'Préparation du commit des docblocks',
	'titre_docblocks_rubrique' => 'Docblocks de la rubrique',
	'titre_langue_docblock' => 'Langue de ce docblock',
	'titre_logo_docblock' => 'Logo de ce docblock',
	
);

?>
