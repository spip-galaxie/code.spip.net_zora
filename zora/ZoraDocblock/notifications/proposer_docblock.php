<?php
/**
 * Fichier gérant la notification d'un docblock proposé
 *
 * @plugin     Zora Docblock
 * @copyright  2013
 * @author     Matthieu Marcillaud
 * @licence    GNU/GPL
 * @package    SPIP\Zoradocblock\Notifications
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Notification d'une proposition de docblock
 *
 * @param string $quoi
 *     Nom de la notification
 * @param int $id
 *     Identifiant de l'objet
 * @param array $options
 *     Options ne notifications
**/
function notifications_proposer_docblock_dist($quoi, $id, $options) {

	include_spip('inc/config');
	include_spip('inc/texte');
	$modele = "notifications/proposer_docblock";

	$destinataires = array();
	$destinataires[] = lire_config('email_webmaster');

	$destinataires = pipeline('notifications_destinataires', array(
		'args'=>array('quoi'=>$quoi,'id'=>$id,'options'=>$options),
		'data'=>$destinataires
	));

	$texte = email_notification_objet($id, "docblock", $modele);
	notifications_envoyer_mails($destinataires, $texte);
}

?>
