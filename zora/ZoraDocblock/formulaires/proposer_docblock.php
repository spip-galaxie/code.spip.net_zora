<?php
/**
 * Gestion du formulaire de d'édition public de docblock
 *
 * @plugin     Zora Docblock
 * @copyright  2013
 * @author     Matthieu Marcillaud
 * @licence    GNU/GPL
 * @package    SPIP\Zoradocblock\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

include_spip('formulaires/editer_docblock');

/** singleton pour transmettre les infos calculées entre C,V et T */
class InfosCVT {
	private static $_instance = null;

	private $valeurs = array();

	public function setValeurs($valeurs) { $this->valeurs = $valeurs; }
	public function getValeurs() { return $this->valeurs; }

	private function __construct() {}

	public static function getInstance() {
		if (is_null(self::$_instance)) {
			self::$_instance = new InfosCVT();
		}
		return self::$_instance;
	}
}

/**
 * Chargement du formulaire de proposition de docblock
 *
 * @param string $fichier
 *     Chemin du fichier
 * @param string $fonction
 *     Nom de la fonction
 * @return array
 *     Environnement du formulaire
 */
function formulaires_proposer_docblock_charger_dist($fichier='', $fonction='') {
	include_spip('inc/session');
	$type_element = $fonction ? 'function' : 'file';
	$valeurs = array(
		'fichier' => $fichier,
		'element' => $fonction,
		'type_element' => $type_element,
		'parent' => '',
		'resume' => '',
		'auteur' => session_get('nom'),
		/*'editable' => true,*/
	);
	if (!$fichier) {
		$valeurs['message_erreur'] = 'Aucun fichier indiqué !';
		$valeurs['editable'] = false;
	} else {
		include_spip('inc/zoracode');
		$docblock = getDocBlock(array(
			'type_element' => $type_element,
			'file' => ZORACODE_SOURCE . $fichier,
			'element' => $fonction)
		);

		if ($docblock['erreur']) {
			$valeurs['message_erreur'] = $docblock['erreur'];
			$valeurs['editable'] = false;
		} else {
			$doc = $docblock['docblock'];
			if (!$doc) {
				$doc = "/**\n *\n */";
			}
			$valeurs['docblock'] = $doc;
			$valeurs['rows_docblock'] = max(substr_count($doc, "\n") + 3, 10);
			if ($docblock['source']) {
				// si vrai docblock, il est déjà dans la source,
				// sinon on ajoute celui calculé old school
				$before = $docblock['annotation'] ? '' : $docblock['docblock'];
				if ($before) $before .= "\n";
				include_spip('inc/texte');
				$valeurs['_source'] = propre("<cadre class='php'>$before$docblock[source]</cadre>");
			}
		}
	}

	return $valeurs;
}

/**
 * Vérifications du formulaire de proposition de docblock
 * 
 * @param string $fichier
 *     Chemin du fichier
 * @param string $fonction
 *     Nom de la fonction
 * @return array
 *     Tableau des erreurs
 */
function formulaires_proposer_docblock_verifier_dist($fichier='', $fonction=''){
	$erreurs = array();
	if (!_request('auteur')) {
		$erreurs['auteur'] = "Vous devez indiquer un nom ou pseudo !";
	}

	$valeurs = formulaires_proposer_docblock_charger_dist($fichier, $fonction);
	if (!$doc = trim(_request('docblock'))) {
		$erreurs['docblock'] = "Aucun docblock !";
	} elseif ($doc == $valeurs['docblock']) {
		$erreurs['docblock'] = "Vous n'avez pas fait de modification !";
	} else {
		foreach (explode("\n", $doc) as $n=>$ligne) {
			if ($ligne[1] != '*') {
				if (!isset($erreurs['docblock'])) $erreurs['docblock'] = '';
				else $erreurs['docblock'] .= '<br />';
				$erreurs['docblock'] .= "Le docblock est mal formé (ligne $n : manque *)";
			}
		}
	}

	$cvt = InfosCVT::getInstance();
	$cvt->setValeurs($valeurs);
	#var_dump($valeurs);

	if (count($erreurs)) {
		$erreurs['message_erreur'] = "Bah ! Y a des choses qui vont pas !";
	} 

	return $erreurs;
}

/**
 * Traitement du formulaire de proposition de docblock
 * 
 * @param string $fichier
 *     Chemin du fichier
 * @param string $fonction
 *     Nom de la fonction
 * @return array
 *     Retours des traitements
 */
function formulaires_proposer_docblock_traiter_dist($fichier='', $fonction=''){
	$doc = trim(_request('docblock'));
	$auteur = trim(_request('auteur'));
	$resume = trim(_request('resume'));

	// éviter les CRLF de windows
	$doc = str_replace("\r\n", "\n", $doc);

	$cvt = InfosCVT::getInstance();
	$valeurs = $cvt->getValeurs();
	#var_dump($valeurs);
	$set = array(
		'fichier' => $valeurs['fichier'],
		'element' => $valeurs['element'],
		'type_element' => $valeurs['type_element'],
		'parent' => $valeurs['parent'],
		'auteur' => $auteur,
		/* on met d'abord l'ancien pour faire une révision avec le nouveau */
		'resume' => '',
		'docblock' => $valeurs['docblock'],
		'statut' => 'prop'
	);

	// avoir un titre tout de même...
	if ($set['type_element'] == 'file') {
		$fichier = end(explode('/', $valeurs['fichier']));
		$set['element'] = "En-tête de $fichier"; 
	}

	$editer_objet = charger_fonction('editer_objet', 'action');
	list($id, $err) = $editer_objet('new', 'docblock', $set);
	if ($id) {
		autoriser_exception("modifier","docblock",$id);
		autoriser_exception("instituer","docblock",$id);
		objet_modifier('docblock', $id, array('docblock' => $doc, 'resume' => $resume, 'statut' => 'prop'));
		autoriser_exception("modifier","docblock",$id,false);
		autoriser_exception("instituer","docblock",$id,false);

		// Notifications
		if ($notifications = charger_fonction('notifications', 'inc', true)) {
			$set['docblock_ancien'] = $valeurs['docblock'];
			$set['docblock'] = $doc;
			$notifications('proposer_docblock', $id, $set);
		}
	}

	$retour = array(
		'editable' => false,
	);
	if ($err)
		$retour['message_erreur'] = $err;
	else
		$retour['message_ok'] = "Votre proposition à été enregistrée. Merci beaucoup !";

	return $retour;
}


?>
