<?php
/**
 * Gestion du formulaire de commit de docblocks
 *
 * @plugin     Zora Docblock
 * @copyright  2013
 * @author     Matthieu Marcillaud
 * @licence    GNU/GPL
 * @package    SPIP\Zoradocblock\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Chargement du formulaire de commit de docblocks
 *
 * @return array
 *     Environnement du formulaire
 */
function formulaires_commiter_docblocks_charger_dist() {
	include_spip('inc/config');
	include_spip('inc/session');
	$presents = lire_config('zora/docblock/appliques', array());
	#if (!$presents) return false;

	$infos = sql_allfetsel('element, fichier, resume, auteur', 'spip_docblocks',
		sql_in('id_docblock', $presents), '', 'fichier ASC, element ASC');

	$message = "Documentation du code :\n";
	foreach ($infos as $i) {
		$message .= "- $i[element] (par $i[auteur]) : $i[resume]\n";
	}
	$valeurs['message_commit'] = $message;

	// svn status
	list($status, $erreurs) = lancer_commande_svn_partout('status');
	if ($erreurs) {
		$valeurs['_svn_status'] = $erreurs;
	} else {
		$valeurs['_svn_status'] = $status;
	}
	$valeurs['_svn_status_rows'] = min(20, substr_count($valeurs['_svn_status'], "\n") + 2);


	// svn diff
	list($diff, $erreurs) = lancer_commande_svn_partout('diff');
	if ($erreurs) {
		$valeurs['_svn_diff'] = $erreurs;
	} else {
		$valeurs['_svn_diff'] = $diff;
	}
	$valeurs['_svn_diff_rows'] = min(20, substr_count($valeurs['_svn_diff'], "\n") + 2);

	$valeurs['message_commit_erreur'] = '';
	$valeurs['message_commit_resultat'] = '';

	$valeurs['relecteur'] = session_get('nom');

	return $valeurs;
}

/**
 * Vérifications du formulaire de commit de docblocks
 * 
 * @return array
 *     Tableau des erreurs
 */
function formulaires_commiter_docblocks_verifier_dist(){
	$erreurs = array();
	if (!_request('message_commit')) {
		$erreurs['message_commit'] = "Vous devez indiquer un message !";
	}

	if (count($erreurs)) {
		$erreurs['message_erreur'] = "Pff ! Y'a des choses qui vont pas !";
	} 

	return $erreurs;
}

/**
 * Traitement du formulaire de commit de docblocks
 * 
 * @return array
 *     Retours des traitements
 */
function formulaires_commiter_docblocks_traiter_dist(){
	#refuser_traiter_formulaire_ajax();

	$retour = array();
	$message = _request('message_commit');
	if ($relecteur = _request('relecteur')) {
		$message .= "\n\nRelecteur : $relecteur";
	}

	// svn commit
	list($res, $err) = lancer_commande_svn_partout('commit', array('message' => $message));
	set_request('message_commit_erreur', $err);
	set_request('message_commit_resultat', $res);
	if (!$err) {

		include_spip('inc/config');
		$presents = lire_config('zora/docblock/appliques');
		effacer_config('zora/docblock/appliques');

		include_spip('action/editer_objet');
		foreach ($presents as $id_docblock) {
			objet_instituer('docblock', $id_docblock, array('statut' => 'publie'));
		}

		$url = generer_url_ecrire('docblocks');

		$retour['message_ok'] = "
			Le commit a été réalisé.<br />
			Vous pouvez retourner <a href='$url'>à la liste des docblocks.</a>";

		$retour['editable'] = false;

	} else {
		$retour['message_erreur'] = "
			Des erreurs sont survenues.<br />
			Tout ou partie a pu être commité.<br />
			Un revert sera peut être nécessaire pour remettre au propre les fichiers source locaux.<br />
			Vous pouvez retourner <a href='$url'>à la liste des docblocks.</a>";

		$retour['editable'] = false;

	}

	return $retour;
}


/**
 * Éxécute la commande indiquée
 * sur chaque partie de SPIP
 *
 * @param string $cmd Commande svn
 * @param string $options Options
 * @return array Liste($retours, $erreurs)
**/
function lancer_commande_svn_partout($cmd, $options=array()) {
	static $dirs = null;

	// récuperation de tous les chemins (une seule fois suffit)
	if (is_null($dirs)) {
		$dirs = array(
			'Core' => realpath(ZORACODE_SOURCE),
			'squelettes-dist' => realpath(ZORACODE_SOURCE . 'squelettes-dist')
		);
		exec('cd ' . escapeshellarg(ZORACODE_SOURCE . 'plugins-dist') . ' && ls', $ls, $erreur);
		if ($erreur) return array('', 'Impossible de lire plugins-dist');
		foreach ($ls as $dir) {
			$dirs[$dir] = realpath(ZORACODE_SOURCE . 'plugins-dist/' . $dir);
		}
	}

	$resultats = array();
	$erreurs = array();

	// parcours tous les chemins, et applique la commande svn
	foreach ($dirs as $cle=>$dir) {
		list($res, $err) = lancer_commande_svn($dir, $cmd, $options, $cle);
		if ($err) {
			$erreurs[$cle] = $err;
		} elseif ($res) { 
			$resultats[$cle] = $res;
		}
	}

	$err = '';
	if (count($erreurs)) {
		foreach ($erreurs as $cle => $e) {
			$err .= "\n\n$cle\n--------\n";
			if (is_string($e)) $err .= $e . "\n";
			else $err .= implode("\n", $e);
		}
		$err = trim($err);
	} else {
		$err = false;
	}

	$res = '';
	if (count($resultats)) {
		foreach ($resultats as $cle => $r) {
			$res .= "\n\n$cle\n----------\n";
			if (is_string($r)) $res .= $r . "\n";
			else $res .= implode("\n", $r);
		}
		$res = trim($res);
	} else {
		$res = false;
	}

	return array($res, $err);
}

/**
 * Éxécute la commande indiquée
 * sur un répertoire de SPIP
 *
 * @param string $dir Répertoire
 * @param string $cmd Commande svn
 * @param string $options Options
 * @param string $cle Précisions pour les erreurs
 * @return array Liste (retours, erreurs)
**/
function lancer_commande_svn($dir, $cmd, $options=array(), $cle = '') {
	// être utf 8 pour pas se faire bouffer les accents !
	$locale = 'fr_FR.UTF-8';
	setlocale(LC_ALL, $locale);
	putenv('LC_ALL='.$locale);

	$cd = "cd " . escapeshellarg($dir);
	$res = array();
	$infos = '';
	$err   = '';
	$svn = defined('ZORACODE_SVN_CMD') ? ZORACODE_SVN_CMD : 'svn';
	switch ($cmd) {
		case 'status':
			exec("$cd && $svn status --ignore-externals", $res, $erreur);
			if ($erreur) {
				$err = "Status en erreur sur $cle : $erreur";
			} else {
				// on enlève :
				// ? : non versionné
				// X : un répertoire non versionné créé par une référence externe
				foreach ($res as $c=>$s) {
					if (in_array($s[0], array('?', 'X'))) {
						unset($res[$c]);
					}
				}
				if ($res) {
					$infos = $res;
				}
			}
			break;
		
		case 'diff':
			exec("$cd && $svn diff", $res, $erreur);
			if ($erreur) {
				$err = "Diff en erreur sur $cle : $erreur";
			} elseif ($res) {
				$infos = $res;
			}
			break;

		case 'revert':
			exec("$cd && $svn revert -R .", $res, $erreur);
			if ($erreur) {
				$err = "Revert en erreur sur $cle : $erreur";
			} elseif ($res) {
				$infos = $res;
			}
			break;

		case 'commit':
			// seulement si changements 
			list($r, $e) = lancer_commande_svn($dir, 'status', array(), $cle);
			if ($r and !$e) {
				$m = $options['message'];
				$m = escapeshellarg( $m );
				#$m = str_replace('!', '\!', $m );

				spip_log("-- Commit sur $cle avec message : $m", 'zoradocblock.3');

				exec("$cd && $svn commit "
					. " --non-interactive"
					. " --username " . escapeshellarg(ZORACODE_SVN_USER)
					. " --password " . escapeshellarg(ZORACODE_SVN_PASS)
					. " -m " . $m . " .",
					$res, $erreur);

				if ($erreur) {
					spip_log("-- Erreur : $erreur", 'zoradocblock.3');
					$err = "Commit en erreur sur $cle : $erreur";
					// les erreurs peuvent être dans la réponse !
					if ($res) $err .= "\n\n" . implode("\n", $res); 
				} elseif ($res) {
					$infos = $res;
				}
			}
			break;

		default:
			$infos = '';
			$err = "Commande '$cmd' inconnue ou non implémentée";
			break;
	}

	return array($infos, $err);
}

?>
