<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin Zora Docblock
 *
 * @plugin     Zora Docblock
 * @copyright  2013
 * @author     Matthieu Marcillaud
 * @licence    GNU/GPL
 * @package    SPIP\Zoradocblock\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Fonction d'installation et de mise à jour du plugin Zora Docblock.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
**/
function zoradocblock_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();

	$maj['create'] = array(array('maj_tables', array('spip_docblocks')));

	# ajout du résumé
	$maj['1.1.0'] = array(
		array('maj_tables', array('spip_docblocks')),
	);

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}


/**
 * Fonction de désinstallation du plugin Zora Docblock.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
**/
function zoradocblock_vider_tables($nom_meta_base_version) {

	sql_drop_table("spip_docblocks");

	# Nettoyer les versionnages et forums
	sql_delete("spip_versions",              sql_in("objet", array('docblock')));
	sql_delete("spip_versions_fragments",    sql_in("objet", array('docblock')));
	sql_delete("spip_forum",                 sql_in("objet", array('docblock')));

	effacer_meta($nom_meta_base_version);
}

?>
