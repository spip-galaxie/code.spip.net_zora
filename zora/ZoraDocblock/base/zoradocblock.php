<?php
/**
 * Déclarations relatives à la base de données
 *
 * @plugin     Zora Docblock
 * @copyright  2013
 * @author     Matthieu Marcillaud
 * @licence    GNU/GPL
 * @package    SPIP\Zoradocblock\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Déclaration des alias de tables et filtres automatiques de champs
 *
 * @pipeline declarer_tables_interfaces
 * @param array $interfaces
 *     Déclarations d'interface pour le compilateur
 * @return array
 *     Déclarations d'interface pour le compilateur
 */
function zoradocblock_declarer_tables_interfaces($interfaces) {

	$interfaces['table_des_tables']['docblocks'] = 'docblocks';

	return $interfaces;
}


/**
 * Déclaration des objets éditoriaux
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function zoradocblock_declarer_tables_objets_sql($tables) {

	$tables['spip_docblocks'] = array(
		'type' => 'docblock',
		'principale' => "oui",
		'field'=> array(
			"id_docblock"        => "bigint(21) NOT NULL",
			"element"            => "tinytext NOT NULL DEFAULT ''",
			"docblock"           => "text NOT NULL DEFAULT ''",
			"fichier"            => "varchar(255) NOT NULL DEFAULT ''",
			"type_element"       => "varchar(25) NOT NULL DEFAULT ''",
			"parent"             => "varchar(255) NOT NULL DEFAULT ''",
			"auteur"             => "varchar(255) NOT NULL DEFAULT ''",
			"resume"             => "tinytext NOT NULL DEFAULT ''",
			"date"               => "datetime NOT NULL DEFAULT '0000-00-00 00:00:00'", 
			"statut"             => "varchar(20)  DEFAULT '0' NOT NULL", 
			"maj"                => "TIMESTAMP"
		),
		'key' => array(
			"PRIMARY KEY"        => "id_docblock",
			"KEY statut"         => "statut", 
		),
		'titre' => "element AS titre, '' AS lang",
		'date' => "date",
		'champs_editables'  => array('element', 'docblock', 'fichier', 'type_element', 'parent', 'resume', 'auteur'),
		'champs_versionnes' => array('docblock', 'resume'),
		'rechercher_champs' => array(),
		'tables_jointures'  => array(),
		'statut_textes_instituer' => array(
			'prepa'    => 'texte_statut_en_cours_redaction',
			'prop'     => 'texte_statut_propose_evaluation',
			'applique' => 'docblock:texte_statut_applique',
			'publie'   => 'texte_statut_publie',
			'refuse'   => 'texte_statut_refuse',
			'poubelle' => 'texte_statut_poubelle',
		),
		'statut_images' => array(
			'prepa'    => 'puce-preparer-8.png',
			'prop'     => 'puce-proposer-8.png',
			'applique' => 'puce-appliquer-8.png',
			'publie'   => 'puce-publier-8.png',
			'refuse'   => 'puce-refuser-8.png',
			'poubelle' => 'puce-supprimer-8.png',
		),
		'statut'=> array(
			array(
				'champ'     => 'statut',
				'publie'    => 'publie',
				'previsu'   => 'publie,prop,prepa,applique',
				'post_date' => 'date', 
				'exception' => array('statut','tout')
			)
		),
		'texte_changer_statut' => 'docblock:texte_changer_statut_docblock', 
		

	);

	return $tables;
}



?>
