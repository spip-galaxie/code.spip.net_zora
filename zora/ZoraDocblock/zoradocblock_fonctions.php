<?php
/**
 * Fonctions utiles au plugin Zora Docblock
 *
 * @plugin     Zora Docblock
 * @copyright  2013
 * @author     Matthieu Marcillaud
 * @licence    GNU/GPL
 * @package    SPIP\Zoradocblock\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Retourne la révision du code source (du core)
 *
 * @return int numéro de révision
**/
function zoracode_revision_source() {
	$svn = defined('ZORACODE_SVN_CMD') ? ZORACODE_SVN_CMD : 'svn';
	exec('cd ' . escapeshellarg(realpath(ZORACODE_SOURCE)) . " && $svn info --xml", $xml, $err);
	if ($err) return "Erreur de chiotte ! Code : " . $err;
	if (!$xml) return "Pas de XML ! Chiotte !";
	$xml = simplexml_load_string(implode("", $xml));
	return $xml->entry['revision'];
}

/**
 * Retourne un docblock sur un élément (du code source)
 *
 * @param string $file
 * @param string $element
 * @param string $type_element
 * @param string $parent
 * @return array
**/
function zoracode_infos_docblock($file, $element, $type_element, $parent) {
	include_spip('inc/zoracode');
	$data = array(
		'file' => ZORACODE_SOURCE . $file,
		'element' => $element,
		'type_element' => $type_element,
		'parent' => $parent,
	);

	$docblock = getDocBlock($data);
	#echo "\n<pre>"; print_r($docblock); echo "</pre>";
	return $docblock;
}

?>
