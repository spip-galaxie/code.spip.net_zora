<?php

/**
 * Retrouver le lien sur SPIP.net
 *
 * @param int $version Code d'une version de SPIP
 * @return string URL de la doc sur spip.net
**/
function spip_lien($version) {
	static $liens = array(
		1 => 1309,
		10 => 1309,
		103 => 1309,
		104 => 1309,
		105 => 1309,
		12 => 1310,
		121 => 1310,
		13 => 1253,
		14 => 1832,
		15 => 1911,
		16 => 1965,
		17 => 2102,
		171 => 2102,
		172 => 2102,
		18 => 2991,
		181 => 2991,
		182 => 3173,
		183 => 3333,
		19 => 3368,
		191 => 3462,
		192 => 3567,
		20 => 3784,
		21 => 4728,
		30 => 5427,
	);


	if (isset($liens[$version])) {
		return "https://www.spip.net/" . $liens[$version];
	}

	return "";
}

/**
 * Retrouver le titre sur SPIP.net
 *
 * @param int $version Code d'une version de SPIP
 * @return string Code HTML du titre de la doc sur spip.net
**/
function spip_titre($version) {
	static $titres = array(
		183 => "<span style='color: #fa9a00;'>SPIP 1.8.3</span>",
		19 => "<span style='color: darkviolet;'>S</span><span style='color: blue;'>P</span><span style='color: green;'>I</span><span style='color: salmon;'>P</span> <span style='color: green;'>1</span><span style='color: blue;'>.</span><span style='color: darkviolet;'>9</span>",
		191 => "<span style='color: darkviolet;'>S</span><span style='color: blue;'>P</span><span style='color: green;'>I</span><span style='color: salmon;'>P</span> <span style='color: green;'>1</span>.<span style='color: darkviolet;'>9</span>.<span style='color: salmon;'>1</span>",
		192 => "<span style='color: darkviolet;'>S</span><span style='color: blue;'>P</span><span style='color: green;'>I</span><span style='color: salmon;'>P</span> <span style='color: green;'>1</span>.<span style='color: darkviolet;'>9</span>.<span style='color: salmon;'>2</span>",
		20 => '<span style="color: #273494;">SPIP 2.0</span>',
		21 => '<span style="color: #169249;">SPIP 2.1</span>',
		30 => '<span style="color:#00CAD8">SPIP 3.0</span>',
	);

	if (isset($titres[$version])) {
		return $titres[$version];
	}

	return "";
}
