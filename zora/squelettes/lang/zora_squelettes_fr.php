<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'aide_404' => 'Peut être cherchiez vous une des pages proposées ci-dessous.',
	'aide_404_recherche' => 'Recherche réalisée à partir du ou des termes : « @recherche@ ».',
	
	'date_derniere_modification' => 'Dernière modification le @date@',
	'date_publication' => 'Publié le @date@',
);
?>
