<?php


/**
 * Retourne le phpdoc demandé
 *
 * @param string $nom
 *     Nom de l'élément
 * @param string $type
 *     Type de l'élément
 * @return array|string
 *     - chaîne d'erreur si erreur,
 *     - phpdoc sinon
**/
function get_phpdoc($nom, $type='fonction') {
	static $xml = null;
	if (is_null($xml)) {
		$xml = find_in_path('autodoc/structure_zora.xml');
	}
	if (!$xml) return "XML non trouvé !";

	$sxml = simplexml_load_file($xml);
	if (!$sxml) {
		return "XML non lisible !";
	}

	if (!$nom) return "Aucun nom demandé";
	if (!$type) return "Aucun type demandé";

	switch ($type) {
		case 'fonction':
			$elements = $sxml->xpath("/project/file/function[name='$nom']");
			if (!$elements) return "Aucun $nom trouvé en $type";
			return zora_simpleXML_to_array($elements[0]);
			break;
	}
}

// https://www.php.net/manual/pt_BR/book.simplexml.php#108688 (17 mai 2012)
function zora_simpleXML_to_array($obj) {
# Cette fonction getDocNamespaces est tres gourmande sur de gros fichiers
# $namespace = $obj->getDocNamespaces(true);

	$namespace[NULL] = NULL;

	$children = array();
	$attributes = array();
	$name = strtolower((string)$obj->getName());

	$text = trim((string)$obj);
	if( strlen($text) <= 0 ) {
		$text = NULL;
	}

	// get info for all namespaces
	if(is_object($obj)) {
		foreach( $namespace as $ns=>$nsUrl ) {
			// atributes
			$objAttributes = $obj->attributes($ns, true);
			foreach( $objAttributes as $attributeName => $attributeValue ) {
				$attribName = strtolower(trim((string)$attributeName));
				$attribVal = trim((string)$attributeValue);
				if (!empty($ns)) {
					$attribName = $ns . ':' . $attribName;
				}
				$attributes[$attribName] = $attribVal;
			}

			// children
			$objChildren = $obj->children($ns, true);
			foreach( $objChildren as $childName=>$child ) {
				$childName = strtolower((string)$childName);
				if( !empty($ns) ) {
					$childName = $ns.':'.$childName;
				}
				$children[$childName][] = zora_simpleXML_to_array($child);
			}
		}
	}

	return array(
		'name'=>$name,
		'text'=>$text,
		'attributes'=>$attributes,
		'children'=>$children
	);
}

/**
 * Retourne le markdown d'un texte
 *
 * @param string $texte
 * @return string
**/
function markdown($texte) {
	static $markdownParser = null;
	if (!class_exists('\dflydev\markdown\MarkdownExtraParser')) {
		include_spip('lib/dflydev/markdown/IMarkdownParser');
		include_spip('lib/dflydev/markdown/MarkdownParser');
		include_spip('lib/dflydev/markdown/MarkdownExtraParser');
	}
	if (is_null($markdownParser)) {
		$markdownParser = new \dflydev\markdown\MarkdownExtraParser();
	}

	$texte = prepare_markdown($texte);
	return $markdownParser->transformMarkdown($texte);
}

/**
 * Traite un texte pour le markdown au besoin
 * 
 * Retourne le markdown d'un texte
 *
 * @param string $texte
 * @return string
**/
function markdownize($texte) {

	// si c'est déjà markdown, on quitte
	if ('<p>' == substr($texte, 0, 3)) {
		return $texte;
	}
	// only transform using markdown if the text contains characters
	// other than word characters, whitespaces and punctuation characters.
	// This is because Markdown is a huge performance hit on the system
	#if (!preg_match('/^[\w|\s|\.|,|;|\:|\&|\#]+$/', $texte)) {
	if (false !== strpos($texte, '`')
	  OR false !== strpos($texte, '[')
	  OR false !== strpos($texte, '- '))
	{
		$texte = markdown($texte);
	} else {
		// markdown will always surround the element with a paragraph;
		// we do the same here to make it consistent
		$texte = '<p>' . $texte . '</p>';
	}

	return $texte;
}


/**
 * Prépare le texte au passage dans markdown
 *
 * - Homogénéise l'indentation du texte. On enlève l'indentation
 *   de chaque ligne. Sauf la première ligne : en effet, phpdocumentor
 *   effectue un trim() sur l'ensemble du texte, ce qui enlève les
 *   espacements sur cette première ligne.
 *
 * - Indente le contenu intérieur d'un code (commence par ``` et fini
 *   par ```) en sautant une ligne et en enlevant les ```
 *
 * @param string $texte Le texte à préparer
 * @return string Texte modifié
**/
function prepare_markdown($texte) {
	// 1)
	// on essaie de calculer le nombre d'espacements à gauche des lignes
	// pour les enlever. Pour cela, il ne faut pas tenir compte de la
	// première ligne, car phpdocumentor effectue un trim()
	$texte = explode("\n", $texte);
	$espace = 10;
	foreach ($texte as $k => $t) {
		if ($k) {
			$esp = strlen($t) - strlen(ltrim($t));
			$espace = min($espace, $esp);
		}
	}

	// on a ici normalement l'espace minimum.
	// on enlève cet espacement sur chaque ligne
	if ($espace) {
		foreach ($texte as $k => $t) {
			if (strlen($t) - strlen(ltrim($t)) > 0) {
				$texte[$k] = substr($t, $espace);
			}
		}
	}

	// 2)
	// gerer un peu les codes (et les listes)
	$open = false;
	foreach ($texte as $k => $t) {
		if (!$open) {
			if (substr(ltrim($t),0,3) === '```') {
				$esp = substr($t,0,strlen($t) - strlen(ltrim($t)));
				$texte[$k] = "\n\n" . $esp . substr(ltrim($t),3);
				$open = true;
			}
		} else {
			$t = '    ' . $t;
			$texte[$k] = $t;
			if (substr(rtrim($t),-3) === '```') {
				$texte[$k] = substr(rtrim($t),0,-3) . "\n";
				$open = false;
			}

		}
	}

	$texte = implode("\n", $texte);
	return $texte;
}
?>
