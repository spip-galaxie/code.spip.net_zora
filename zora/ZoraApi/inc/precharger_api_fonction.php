<?php
/**
 * Préchargement des formulaires d'édition de api_fonction
 *
 * @plugin     Zora Api
 * @copyright  2013
 * @author     Matthieu Marcillaud
 * @licence    GNU/GPL
 * @package    SPIP\Zoraapi\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

include_spip('inc/precharger_objet');

/**
 * Retourne les valeurs à charger pour un formulaire d'édition d'un api_fonction
 *
 * Lors d'une création, certains champs peuvent être préremplis
 * (c'est le cas des traductions) 
 *
 * @param string|int $id_api_fonction
 *     Identifiant de api_fonction, ou "new" pour une création
 * @param int $id_rubrique
 *     Identifiant éventuel de la rubrique parente
 * @param int $lier_trad
 *     Identifiant éventuel de la traduction de référence
 * @return array
 *     Couples clés / valeurs des champs du formulaire à charger.
**/
function inc_precharger_api_fonction_dist($id_api_fonction, $id_rubrique=0, $lier_trad=0) {
	return precharger_objet('api_fonction', $id_api_fonction, $id_rubrique, $lier_trad, 'nom');
}

/**
 * Récupère les valeurs d'une traduction de référence pour la création
 * d'un api_fonction (préremplissage du formulaire). 
 *
 * @note
 *     Fonction facultative si pas de changement dans les traitements
 * 
 * @param string|int $id_api_fonction
 *     Identifiant de api_fonction, ou "new" pour une création
 * @param int $id_rubrique
 *     Identifiant éventuel de la rubrique parente
 * @param int $lier_trad
 *     Identifiant éventuel de la traduction de référence
 * @return array
 *     Couples clés / valeurs des champs du formulaire à charger
**/
function inc_precharger_traduction_api_fonction_dist($id_api_fonction, $id_rubrique=0, $lier_trad=0) {
	return precharger_traduction_objet('api_fonction', $id_api_fonction, $id_rubrique, $lier_trad, 'nom');
}


?>