<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin Zora Api
 *
 * @plugin     Zora Api
 * @copyright  2013
 * @author     Matthieu Marcillaud
 * @licence    GNU/GPL
 * @package    SPIP\Zoraapi\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Fonction d'installation et de mise à jour du plugin Zora Api.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
**/
function zoraapi_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();

	$maj['create'] = array(array('maj_tables', array('spip_api_fonctions')));
	# ajout de id_secteur, finalement
	include_spip('zoraapi_pipelines');
	$maj['1.1.0'] = array(
		array('maj_tables', array('spip_api_fonctions')),
		array('sql_alter', 'TABLE spip_api_fonctions ADD INDEX (id_secteur)'),
		array('zoraapi_trig_propager_les_secteurs', '')
	);
	# ajout de la date de modification
	$maj['1.2.0'] = array(
		array('maj_tables', array('spip_api_fonctions')),
		array('sql_update', 'spip_api_fonctions', array('date_modif' => 'date_publication')),
	);

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}


/**
 * Fonction de désinstallation du plugin Zora Api.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
**/
function zoraapi_vider_tables($nom_meta_base_version) {

	sql_drop_table("spip_api_fonctions");

	# Nettoyer les versionnages et forums
	sql_delete("spip_versions",              sql_in("objet", array('api_fonction')));
	sql_delete("spip_versions_fragments",    sql_in("objet", array('api_fonction')));
	sql_delete("spip_forum",                 sql_in("objet", array('api_fonction')));
	sql_delete("spip_urls",                  sql_in("objet", array('api_fonction')));

	effacer_meta($nom_meta_base_version);
}

?>
