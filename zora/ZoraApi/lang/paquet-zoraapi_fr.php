<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// Z
	'zoraapi_description' => 'Gère les fonctions d\'API dans un objet éditorial',
	'zoraapi_nom' => 'Zora Api',
	'zoraapi_slogan' => 'Gestion des fonctions d\'API',
);

?>