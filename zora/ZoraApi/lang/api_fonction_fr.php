<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_api_fonction' => 'Ajouter cette fonction d\'api',

	// D
	'dernieres_fonctions' => 'Dernières fonctions d\'API',

	// E
	'explication_compatibilite' => 'Exemple : «SPIP >= 3.0»',
	'explication_description' => 'Décrit la fonction et sa signature',
	'explication_fichier' => 'Chemin du fichier depuis la racine de SPIP, contenant la fonction. Exemple « ecrire/inc/utils.php »',
	'explication_historique' => 'Historique au fil des versions de SPIP',
	'explication_nom_complet' => 'Si nécessaire, le nom complet d\'utilisation. Exemple « Namespaces\\NomClass::methode »',
	'explication_voir_aussi' => 'Liens vers d\'autres fonctions',

	// F
	'fonctions_dans_cette_api' => 'Fonctions dans cette API',

	// I
	'icone_creer_api_fonction' => 'Créer une fonction d\'API',
	'icone_modifier_api_fonction' => 'Modifier cette fonction d\'API',
	'info_1_api_fonction' => 'Une fonction d\'API',
	'info_api_fonctions_auteur' => 'Les fonctions d\'API de cet auteur',
	'info_aucun_api_fonction' => 'Aucune fonction d\'API',
	'info_nb_api_fonctions' => '@nb@ fonctions d\'API',
	'info_api_fonctions_valider' => 'Fonctions d\'API à valider',

	// L
	'label_compatibilite' => 'Plage de compatibilité',
	'label_definition' => 'Définition courte',
	'label_description' => 'Description',
	'label_exemples' => 'Exemples',
	'label_fichier' => 'Fichier conteneur',
	'label_historique' => 'Historique',
	'label_nom' => 'Nom',
	'label_nom_complet' => 'Nom complet',
	'label_nota' => 'Notes',
	'label_parametres' => 'Liste des paramètres',
	'label_retour' => 'Valeurs de retour',
	'label_voir_aussi' => 'Voir aussi',
	'liste_des_apis' => 'Liste des APIs',

	// M
	'meme_api' => 'Dans la même API',

	// R
	'retirer_lien_api_fonction' => 'Retirer cette fonction d\'API',
	'retirer_tous_liens_api_fonctions' => 'Retirer toutes les fonctions d\'API',
	'repondre_fonction' => 'Commenter cette fonction d\'API',

	// S
	'statut_toutes' => 'Toutes',
	'statut_proposees' => 'Proposées',
	'statut_publiees' => 'Publiées',
	'statut_miennes' => 'Mes fonctions d\'API',

	// T
	'texte_ajouter_api_fonction' => 'Ajouter une fonction d\'API',
	'texte_changer_statut_api_fonction' => 'Cette fonction d\'API est :',
	'texte_creer_associer_api_fonction' => 'Créer et associer une fonction d\'API',
	'titre_api_fonction' => 'Fonction d\'API',
	'titre_api_fonctions' => 'Fonctions d\'API',
	'titre_api_fonctions_rubrique' => 'Fonctions d\'API de la rubrique',
	'titre_langue_api_fonction' => 'Langue de cette fonction d\'API',
	'titre_logo_api_fonction' => 'Logo de cette fonction d\'API',
);

?>
