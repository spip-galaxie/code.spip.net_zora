<?php
/**
 * Utilisations de pipelines par Zora Api
 *
 * @plugin     Zora Api
 * @copyright  2013
 * @author     Matthieu Marcillaud
 * @licence    GNU/GPL
 * @package    SPIP\Zoraapi\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Ajouter les objets sur les vues de rubriques
 *
 * @pipeline affiche_enfants
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
**/
function zoraapi_affiche_enfants($flux) {
	if ($e = trouver_objet_exec($flux['args']['exec'])
		AND $e['type'] == 'rubrique'
		AND $e['edition'] == false) {

		$id_rubrique = $flux['args']['id_rubrique'];
		$lister_objets = charger_fonction('lister_objets', 'inc');

		$bouton = '';
		if (autoriser('creerapi_fonctiondans', 'rubrique', $id_rubrique)) {
			$bouton .= icone_verticale(_T("api_fonction:icone_creer_api_fonction"), generer_url_ecrire("api_fonction_edit", "id_rubrique=$id_rubrique"), "api_fonction-24.png", "new", "right")
					. "<br class='nettoyeur' />";
		}

		$flux['data'] .= $lister_objets('api_fonctions', array('titre'=>_T('api_fonction:titre_api_fonctions_rubrique') , 'id_rubrique'=>$id_rubrique, 'par'=>'nom'));
		$flux['data'] .= $bouton;

	}
	return $flux;
}


/**
 * Ajout de contenu sur certaines pages,
 * notamment des formulaires de liaisons entre objets
 *
 * @pipeline affiche_milieu
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function zoraapi_affiche_milieu($flux) {
	$texte = "";
	$e = trouver_objet_exec($flux['args']['exec']);

	// auteurs sur les api_fonctions
	if (!$e['edition'] AND in_array($e['type'], array('api_fonction'))) {
		$texte .= recuperer_fond('prive/objets/editer/liens', array(
			'table_source' => 'auteurs',
			'objet' => $e['type'],
			'id_objet' => $flux['args'][$e['id_table_objet']]
		));
	}



	if ($texte) {
		if ($p=strpos($flux['data'],"<!--affiche_milieu-->"))
			$flux['data'] = substr_replace($flux['data'],$texte,$p,0);
		else
			$flux['data'] .= $texte;
	}

	return $flux;
}


/**
 * Afficher le nombre de fonctions d'api dans chaque rubrique
 *
 * @param array $flux
 * @return array
 */
function zoraapi_boite_infos($flux){
	if (
		isset($flux['args']['type']) and $flux['args']['type'] == 'rubrique'
		and isset($flux['args']['id']) and  $id_rubrique = $flux['args']['id']
	){
		if ($nb = sql_countsel('spip_api_fonctions',"statut='publie' AND id_rubrique=".intval($id_rubrique))){
			$nb = "<div>". singulier_ou_pluriel($nb, "api_fonction:info_1_api_fonction", "api_fonction:info_nb_api_fonctions") . "</div>";
			if ($p = strpos($flux['data'],"<!--nb_elements-->"))
				$flux['data'] = substr_replace($flux['data'],$nb,$p,0);
		}
	}
	return $flux;
}

/**
 * Compter les fonctions d'api dans une rubrique
 * 
 * @param array $flux
 * @return array
 */
function zoraapi_objet_compte_enfants($flux){
	if ($flux['args']['objet']=='rubrique'
	  AND $id_rubrique=intval($flux['args']['id_objet'])) {
		// juste les publies ?
		if (array_key_exists('statut', $flux['args']) and ($flux['args']['statut'] == 'publie')) {
			$flux['data']['api_fonction'] = sql_countsel('spip_api_fonctions', "id_rubrique=".intval($id_rubrique)." AND (statut='publie')");
		} else {
			$flux['data']['api_fonction'] = sql_countsel('spip_api_fonctions', "id_rubrique=".intval($id_rubrique)." AND (statut='publie' OR statut='prop')");
		}
	}
	return $flux;
}


/**
 * Ajoute le nombre de fonctions d'api sur l'accueil privé
 * 
 * @pipeline accueil_informations
 * 
 * @param string $texte
 *     HTML des informations générales concernant chaque type d'objet
 *     sur la page d'accueil privée
 * @return string
 *     HTML des informations générales concernant chaque type d'objet
 *     sur la page d'accueil privée
 */
function zoraapi_accueil_informations($texte){
	include_spip('base/abstract_sql');

	$q = sql_select("COUNT(*) AS cnt, statut", 'spip_api_fonctions', '', 'statut', '','', "COUNT(*)<>0");

	$cpt = array();
	$cpt2 = array();
	$where = false;
	if ($GLOBALS['visiteur_session']['statut']=='0minirezo'){
		$where = sql_allfetsel('id_objet','spip_auteurs_liens',"objet='rubrique' AND id_auteur=".intval($GLOBALS['visiteur_session']['id_auteur']));
		if ($where){
			$where = sql_in('id_rubrique',array_map('reset',$where));
		}
	}
	$defaut = $where ? '0/' : '';
	while($row = sql_fetch($q)) {
	  $cpt[$row['statut']] = $row['cnt'];
	  $cpt2[$row['statut']] = $defaut;
	}

	if ($cpt) {
		if ($where) {
			$q = sql_select("COUNT(*) AS cnt, statut", 'spip_api_fonctions', $where, "statut");
			while($row = sql_fetch($q)) {
				$r = $row['statut'];
				$cpt2[$r] = intval($row['cnt']) . '/';
			}
		}
		$texte .= "<div class='accueil_informations api_fonctions liste'>";
		$texte .= "<h4>" . afficher_plus_info(generer_url_ecrire("api_fonctions"), "", _T('api_fonction:titre_api_fonctions')) . "</h4>";
		$texte .= "<ul class='liste-items'>";
		if (isset($cpt['prop'])) $texte .= "<li class='item'>"._T("texte_statut_attente_validation").": ".$cpt2['prop'].$cpt['prop'] . '</li>';
		if (isset($cpt['publie'])) $texte .= "<li class='item on'>"._T("texte_statut_publies").": ".$cpt2['publie'] .$cpt['publie'] . '</li>';
		$texte .= "</ul>";
		$texte .= "</div>";
	}
	return $texte;
}



/**
 * Ajouter les fonctions d'API à valider sur la page d'accueil 
 *
 * @pipeline accueil_encours
 * 
 * @param string $flux  HTML du bloc encours sur la page d'accueil privée
 * @return string       HTML du bloc encours sur la page d'accueil privée
**/
function zoraapi_accueil_encours($flux){
	$lister_objets = charger_fonction('lister_objets','inc');


	$flux .= $lister_objets('api_fonctions', array(
		'titre'=>afficher_plus_info(generer_url_ecrire('api_fonctions'))._T('api_fonction:info_api_fonctions_valider'),
		'statut'=>array('prop', 'prepa'),
		'par'=>'date_publication'));

	return $flux;
}


/**
 * Réajuster les secteurs défectueux
 *
 * @param string $flux
 * @return string
**/
function zoraapi_trig_propager_les_secteurs($flux) {
	$r = sql_select(
		"fille.id_api_fonction AS id, maman.id_secteur AS secteur",
		"spip_api_fonctions AS fille, spip_rubriques AS maman",
		"fille.id_rubrique = maman.id_rubrique AND fille.id_secteur <> maman.id_secteur");
	while ($row = sql_fetch($r))
		sql_update("spip_api_fonctions", array("id_secteur" => $row['secteur']), "id_api_fonction=".$row['id']);
	return $flux;
}
?>
