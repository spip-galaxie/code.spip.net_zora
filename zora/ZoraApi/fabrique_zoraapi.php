<?php

/**
 *  Fichier généré par la Fabrique de plugin v5
 *   le 2013-03-20 09:48:24
 *
 *  Ce fichier de sauvegarde peut servir à recréer
 *  votre plugin avec le plugin «Fabrique» qui a servi à le créer.
 *
 *  Bien évidemment, les modifications apportées ultérieurement
 *  par vos soins dans le code de ce plugin généré
 *  NE SERONT PAS connues du plugin «Fabrique» et ne pourront pas
 *  être recréées par lui !
 *
 *  La «Fabrique» ne pourra que régénerer le code de base du plugin
 *  avec les informations dont il dispose.
 *
**/

if (!defined("_ECRIRE_INC_VERSION")) return;

$data = array (
  'fabrique' => 
  array (
    'version' => 5,
  ),
  'paquet' => 
  array (
    'nom' => 'Zora Api',
    'slogan' => 'Gestion des fonctions d\'API',
    'description' => 'Gère les fonctions d\'API dans un objet éditorial',
    'prefixe' => 'zoraapi',
    'version' => '1.0.0',
    'auteur' => 'Matthieu Marcillaud',
    'auteur_lien' => '',
    'licence' => 'GNU/GPL',
    'categorie' => 'divers',
    'etat' => 'dev',
    'compatibilite' => '[3.0.0-dev;3.1.*]',
    'documentation' => '',
    'administrations' => 'on',
    'schema' => '1.0.0',
    'formulaire_config' => '',
    'formulaire_config_titre' => '',
    'inserer' => 
    array (
      'paquet' => '',
      'administrations' => 
      array (
        'maj' => '',
        'desinstallation' => '',
        'fin' => '',
      ),
      'base' => 
      array (
        'tables' => 
        array (
          'fin' => '',
        ),
      ),
    ),
    'scripts' => 
    array (
      'pre_copie' => '',
      'post_creation' => '',
    ),
    'exemples' => '',
  ),
  'objets' => 
  array (
    0 => 
    array (
      'nom' => 'Fonctions d\'API',
      'nom_singulier' => 'Fonction d\'API',
      'genre' => 'feminin',
      'logo_variantes' => 'on',
      'table' => 'spip_api_fonctions',
      'cle_primaire' => 'id_api_fonction',
      'cle_primaire_sql' => 'bigint(21) NOT NULL',
      'table_type' => 'api_fonction',
      'champs' => 
      array (
        0 => 
        array (
          'nom' => 'Nom',
          'champ' => 'nom',
          'sql' => 'tinytext NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
            2 => 'obligatoire',
          ),
          'recherche' => '7',
          'saisie' => 'input',
          'explication' => '',
          'saisie_options' => '',
        ),
        1 => 
        array (
          'nom' => 'Définition courte',
          'champ' => 'definition',
          'sql' => 'tinytext NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '3',
          'saisie' => 'textarea',
          'explication' => '',
          'saisie_options' => 'conteneur_class=haut,  rows=3',
        ),
        2 => 
        array (
          'nom' => 'Plage de compatibilité',
          'champ' => 'compatibilite',
          'sql' => 'tinytext NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '',
          'saisie' => 'input',
          'explication' => 'Exemple : «SPIP >= 3.0»',
          'saisie_options' => '',
        ),
        3 => 
        array (
          'nom' => 'Nom complet',
          'champ' => 'nom_complet',
          'sql' => 'tinytext NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '',
          'saisie' => 'input',
          'explication' => 'Si nécessaire, le nom complet d\'utilisation. Exemple « Namespaces\\NomClass::methode »',
          'saisie_options' => '',
        ),
        4 => 
        array (
          'nom' => 'Fichier conteneur',
          'champ' => 'fichier',
          'sql' => 'tinytext NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '',
          'saisie' => 'input',
          'explication' => 'Chemin du fichier depuis la racine de SPIP, contenant la fonction. Exemple « ecrire/inc/utils.php »',
          'saisie_options' => '',
        ),
        5 => 
        array (
          'nom' => 'Description',
          'champ' => 'description',
          'sql' => 'mediumtext NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '',
          'saisie' => 'textarea',
          'explication' => 'Décrit la fonction et sa signature',
          'saisie_options' => 'conteneur_class=haut, class=inserer_barre_edition, rows=4',
        ),
        6 => 
        array (
          'nom' => 'Liste des paramètres',
          'champ' => 'parametres',
          'sql' => 'mediumtext NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '',
          'saisie' => 'textarea',
          'explication' => '',
          'saisie_options' => 'conteneur_class=haut, class=inserer_barre_edition, rows=4',
        ),
        7 => 
        array (
          'nom' => 'Valeurs de retour',
          'champ' => 'retour',
          'sql' => 'mediumtext NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '',
          'saisie' => 'textarea',
          'explication' => '',
          'saisie_options' => 'conteneur_class=haut, class=inserer_barre_edition, rows=4',
        ),
        8 => 
        array (
          'nom' => 'Historique',
          'champ' => 'historique',
          'sql' => 'mediumtext NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '',
          'saisie' => 'textarea',
          'explication' => 'Historique au fil des versions de SPIP',
          'saisie_options' => ' conteneur_class=haut, class=inserer_barre_edition, rows=4',
        ),
        9 => 
        array (
          'nom' => 'Exemples',
          'champ' => 'exemples',
          'sql' => 'text NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '',
          'saisie' => 'textarea',
          'explication' => '',
          'saisie_options' => 'conteneur_class=haut, class=inserer_barre_edition, rows=4',
        ),
        10 => 
        array (
          'nom' => 'Notes',
          'champ' => 'nota',
          'sql' => 'mediumtext NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '',
          'saisie' => 'textarea',
          'explication' => '',
          'saisie_options' => 'conteneur_class=haut, class=inserer_barre_edition, rows=4',
        ),
        11 => 
        array (
          'nom' => 'Voir aussi',
          'champ' => 'voir_aussi',
          'sql' => 'mediumtext NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '',
          'saisie' => 'textarea',
          'explication' => 'Liens vers d\'autres fonctions',
          'saisie_options' => 'conteneur_class=haut, class=inserer_barre_edition, rows=4',
        ),
      ),
      'champ_titre' => 'nom',
      'rubriques' => 
      array (
        0 => 'id_rubrique',
        1 => 'vue_rubrique',
      ),
      'langues' => 
      array (
        0 => 'lang',
        1 => 'id_trad',
      ),
      'champ_date' => 'date_publication',
      'statut' => 'on',
      'chaines' => 
      array (
        'titre_objets' => 'Fonctions d\'api',
        'titre_objet' => 'Fonction d\'api',
        'info_aucun_objet' => 'Aucune fonction d\'api',
        'info_1_objet' => 'Une fonction d\'api',
        'info_nb_objets' => '@nb@ fonctions d\'api',
        'icone_creer_objet' => 'Créer une fonction d\'api',
        'icone_modifier_objet' => 'Modifier cette fonction d\'api',
        'titre_logo_objet' => 'Logo de cette fonction d\'api',
        'titre_langue_objet' => 'Langue de cette fonction d\'api',
        'titre_objets_rubrique' => 'Fonctions d\'api de la rubrique',
        'info_objets_auteur' => 'Les fonctions d\'api de cet auteur',
        'retirer_lien_objet' => 'Retirer cette fonction d\'api',
        'retirer_tous_liens_objets' => 'Retirer toutes les fonctions d\'api',
        'ajouter_lien_objet' => 'Ajouter cette fonction d\'api',
        'texte_ajouter_objet' => 'Ajouter une fonction d\'api',
        'texte_creer_associer_objet' => 'Créer et associer une fonction d\'api',
        'texte_changer_statut_objet' => 'Cette fonction d\'api est :',
      ),
      'table_liens' => '',
      'roles' => '',
      'auteurs_liens' => 'on',
      'vue_auteurs_liens' => '',
      'echafaudages' => 
      array (
        0 => 'prive/squelettes/contenu/objets.html',
        1 => 'prive/objets/infos/objet.html',
        2 => 'prive/squelettes/contenu/objet.html',
      ),
      'autorisations' => 
      array (
        'objet_creer' => '',
        'objet_voir' => '',
        'objet_modifier' => '',
        'objet_supprimer' => '',
        'associerobjet' => '',
      ),
      'boutons' => 
      array (
        0 => 'menu_edition',
        1 => 'outils_rapides',
      ),
      'saisies' => 
      array (
        0 => 'objets',
      ),
    ),
  ),
  'images' => 
  array (
    'paquet' => 
    array (
      'logo' => 
      array (
        0 => 
        array (
          'extension' => 'png',
          'contenu' => 'iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAMAAAD04JH5AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAYBQTFRF/bskzIwh/uGk/8kG/9UywoQb/b4es3kU/9tS/9Y4/+iZ/9pL/95i/91a/9lE/9Qr/+/H/+N6/9Eb/+FzvoEZ/8wB/9Ij/8sD//no/sYOrXQQ/LtU/+SC/80F/Lct+7dJ/+Bt/sIW/84J/LUx+7M2/88R/9AV/+WM/84M/Lkp6rtj2KtO+7E5+deI/cZU0pAk/sk33Zkr1JIl/cAZ/Mwr2Kgn/MZt/dya/sgI2qMZ/sQRt3wW0Z4e/cFK2ZYo/scK88d27LUX6LglsHYS/cxj4rJW/99n0Z07/MFh1pMm/dZ1yYog/tVN7cx26cJsypUo/tJTxYcd9s6B4qlF8rwIzJQUz44j/sMTwowWyZc37LUK/dI9u38Y5Kgd350v+8YD/clo57tN/Lwz/coZ15Um46wK2pse/9c+/MA96KMs9MEX5bo31ZwQ970S/dFr+8ATuIAX989T99Bd/s9a36M+/soQ8MQ676kxyI0T+8Nn4bpf/sgX1ZUh+7A7/8wA////yg4fqwAAAIB0Uk5T/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////wA4BUtnAAALxElEQVR42rSbiSMb6xqHp5OEZIIgCVH7llhuuCFEpJZEbA1xCKoVS1HcHj2c29Zpq6P/+v2+Wb91FnV//8DzzPu+3zIThF+8xBoTHR0dTU1N4XBPT8/KSl1dfX293+8PBoMNDQ1XV4FAwOsNgbSANDc3x+PxwcGhob6+zs7OaPQRJhptVeLxS1yMwMcnnk/A4/HGXApICUwA8m0FBi0EZs4bXQnA5/9tgSgqMHMYcyEQkzSBJo7AFRTwGgJ6B6wEFjpcCDRiHQijI+DXCuBeIO1CQOILBJ8ssBBzIUCMgJUAvgisBBpdCnTwR0CfQS97FRICHvcCCXYHnG4DnfoqfG4BP7YI/t8CSAcczCC2DbQ+o0CYMwIOFsFvCrALYD2D/FXoXqCDXQBkG2KOgNUqdCXQwSgAtwM2M6gLtLkUaGIXgOgAawQ4M+hGAOVzRtB2BH5TQONzCsDrwPMLUBPA6IDNRqyOgK1ADAu7AUQHQpwOtHJmsE1qRIMKxNQLkL4BK3iDzy9AulDIwuTzH510oK1tdvbNm97eycnJmpoaQRgTNQEST/GZBXgVeWlmaekgrxTAmYDCF4aHxxoVAQnHM/lYAeAI3mN4kFyQvwsgAr2oQNdZIxBoxOgqXuP3oHy8AAWCv7QkBZgdMPZBlkDX2C8h1oHGwPP4+hosEPilWslv1wFVYBIV6BeFRpJu8FdIProEpkh+bdLPW4TsGVQF1gSJoCt4nI8XQF0CWQJfW7tZz+8AZwSAQL9gkjE8l48KIHggEEYKwNgF2AIDAkVn8/882DpET4E9/PFBymFkDbzb29ulR4DuACoQxvE4fws8r+9e5SsCB/jjgyyHzQ58yFWr1Q8ORgAIhLH0IHiTv+FTOl57b54CByS/dlkyOnBTVXJzYrMISYEeA4/z7+/0NVfQN+GpHIHP5d6WtXPgRONXq99ObBZh/4Am0GOExvvva81NZwrw0wUf9fg5mMjNLijAyTedH4lsnWNrgCnQ00PQFTyH//Ll1uFULdX+nJG93d1vBh5k64HTAUpgZQWhq3gWH1n9tQx+tVpF8dDg3GoR9g/8S1hBUkfh9fmj8EuO8CCrLZYdMATq6jC6hg8q688tH8H7fL7Rc0YBEIE6NPX1BH/Khm+LB3n/wC6A0gFZoOEaHe6+9zbtd4D3+Ur/teiALNST8Zt8qgB2fJoOs7nB7wAp4EfxwYY/7fhvQXw0H8X7SskmfgcQAT+SYFA7/rf4/K3lzYSkZvPHKhfv296UetAC4B2QBT+VoIEHecXm340mJSmxEvS2gJvgYOiqPvyjxORvb5elpge1AMgmwBcIYvirq0Mfi78Mnn0lpL4LaFfReMP7FyR+G2RZSqTZHVD4qEAwiNPh6Q8M7ij+W/D09S1xjA9PwXSFwm+PShLk80YQCASpoHhw+ql7McJflqRwKG7ykYtQoYTjt1cT0iF3E2AJNDSgeOX6ESjg/LIkBeNs/uPj7jaK395KSPVtWAGIEUQFGpAgeHD83hF8b3PcfBnE+NGoYrCtZ0SqY21CmEADlSsMr9zAeXxtAFrNmyg0MPigAg/sAmibAC1whdIVvNd7h/BHWXykAOAqvGvyS6Vy2qYAiMCVGRQfKiD8LUkKxLkDoF3Fswa+VFoNcwugC1wRCQQwfCi0hwxAUqq35CsCJy90PEgyTRQAH0FCIBCg8KH0EtqADnt+a+s7A18qLdchmyCjALIQoOM18aHQEbIBJpUGDBJ8vAHwXWDf4L+YT1gXgBLQ4N77wlE2++rg4ADhj0pNFL+VLoDH807HK/l5eZk9OjpiFwAR8CIJTWEngP7yBQtgywcCCy9MPMw+zE+BVQBZ8FKBhT9gnEBVKeGM7/FkKTxI5ppRAEIgpKeWUYBVqU7D2/Fn8iUKv7+fLJJ7gCkQIsPg535IIWd8z8wtjd8f33nNKIAshBhpSTP4uU0JK7/JfyT5MzM0fnx8JMkoAEMAfnssUALg/pdoQh/fXH9UA8C70CWGH1cykmEUABNoMVJgFCAnrTjmQwECDwREBl8WWlgpMPg5ya/gbfjqy+gliZ+YmFAE+h0JNOfpBigCOt7Yfzn8tksKDwUYBaAEmpXkGQXI6R8CifGjBhC+i15SeFVA4XMEmpHkGfxc0m88vh1fFcDw6+uGgIwJNDMS/0g3ACzDemd8KEDjFQGaTwso522cUYDqjzCOR9tP8D9pfAS/vr4o9lsKxNEwClBd7XDKb8ujj6/iLy52Mgy+LMQZGRw8oPlVnxQn8Dz+bJbGX1xkkgw+JTCoZo8UgC+/5YBD/q3BN/Hz86K1wCCaod2DO7IA1erqyqMj/mwee3yI/1Kp7IinDL4sDJIZUtMkScnNzfLyqvkFIpKMM/AkHwj8NB//eGdnR1ST7GbwMYEhM2DLGQSnA3hj/dv8AhJZDTri55Hyj4ivi8WxublrOIBMgSEqfVrUVd/5zShAJLLZR+IZ/Nt1g39xkRTPtEO4n8knBPr6CDrIB5Mf2fJHGY+P898cId0/FtVLQBePLwt9VDqRgF3/JGIKREZb6Mcn+Hl0+hbFosl3INDZSdDhws8iH6B87/sY5Uf5n9aN8l/MpzKZM53PLoAsdHKi0UFOIoYA/OzIfHw2fx6MoMrnNYAjoML1TWc3YhYApMAvP8mfz4hnw9Z8SkBjGzseKMABWgAQ+EOIicf4+QmMP/+laD0AugB8oKiZRzTq7w/oB7jtvV02/zZr8ufVVK5t+LIQxfJI5aZKFAC+e2dv6fbfHq1T/Pn55TOLAVQEHi0TpfjaB5jsLvr4bbOfjibGaX4qlRoZsORbCcCKfMixCqB8fxjP5j+p/E/5o0vt+KP4qVS7JZ8roHbkXa5KTYDx+aWkvnvr108OP3X83YrPEkBGYo9TAOTt37j+agIXhkBKS2XNoUCUCFgaqoBFARzwU8tiu5VAlBlts8E3QR6f2QCDf5xxKdCK5p0TAa0AbH5qUUzKTgRa2fngmD/B5o+IGdlSoNUmN04EyAaY/Ipo2QA7Abjf3WACq8nRbYsCHJePMYFKRrRcA1YCHj03CH9Ukur+Ht1nF+Cisgiunsc4f0e2FRjis9Uj58YQWJbg7w/ndeUKPYFfykmAHxvuOnbDhwItPLJ+4M1kNX5ZShwqB8/DPV0AUcz8da3cv/6jbwAO+LLg8YQ8nMxoWVjIw58hSpvw9yd47oKDn16C4l+Cfv/8R5t/cVp2IuCdsYArB+7Cwsd9Xymp/v4Fv36/6aUKkNKun8r955/j1PGOmFmzo3d3y8K5p6WFw9bgyon30CMpv/+p+F5dwCxARSwa/IEBeTEjZtptH38N/mDh+drwFeeicO28B0mnHww8FCCWQEWc6zLuv+D8m3ZQ/m6gKEgzM6GNcxYagaudh3QVPzn5ExVQliAU6Df4TtINHNeEGIAfbqS/IlwSrtJR/OQkeQ24GAEC/TbXLzzt092yLAq/OgDMu7FxSIZDV/BMAcvH714j8n0a8qfhHzSugOc9P9wgQ9BRfE1NzSXRgYuyaFn+9mkia3BE12LK35SG26ikNzZmZ9l05cefGuEyha7BClj0luVvZw3ldEz/o9aVdBve8/QffxhwjY7i4a8vr8Xkjpqk8v1hzXL6aIHuRZH/d8VQAINrdBMPcz1XVHJ6OjY3YDP9UMDFHza/+TcU6GXTNTzceJWtx9Hie4IADjfoHLzd4ncp0KsJ8OgY3tHe8wSBSS7dPd61wKQuUFPDfHgM72jrdS3w+TMKxx/ePd61QA0QoOEq/Sn4pwsITLpr/NMEBA79CXjXAsL1588WdNd49wJnnz9fI3CK7hIPzr3p7+4EisWx4WEG/Wl4cPmZXnMjMDw8VixeI3Cc7hYPCwCuHy4EisPX4JS7fia6wv8uyy7+5TPZ1QVKUJzr1+Eq/Yn4bnD9AgVod/NvvwA8dnoKD3ol7b8TiFfun27+8TkDHnru1Mj0bwa+Iq25+9fvHVj27vbniazeP10J/MroTX9S25n3T3cCv2LJtWeCy+07jVzM/wQYAFew4WbyXXL+AAAAAElFTkSuQmCC',
        ),
      ),
    ),
    'objets' => 
    array (
      0 => 
      array (
        'logo' => 
        array (
          0 => 
          array (
            'extension' => 'png',
            'contenu' => 'iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAMAAAD04JH5AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAYBQTFRF/bskzIwh/uGk/8kG/9UywoQb/b4es3kU/9tS/9Y4/+iZ/9pL/95i/91a/9lE/9Qr/+/H/+N6/9Eb/+FzvoEZ/8wB/9Ij/8sD//no/sYOrXQQ/LtU/+SC/80F/Lct+7dJ/+Bt/sIW/84J/LUx+7M2/88R/9AV/+WM/84M/Lkp6rtj2KtO+7E5+deI/cZU0pAk/sk33Zkr1JIl/cAZ/Mwr2Kgn/MZt/dya/sgI2qMZ/sQRt3wW0Z4e/cFK2ZYo/scK88d27LUX6LglsHYS/cxj4rJW/99n0Z07/MFh1pMm/dZ1yYog/tVN7cx26cJsypUo/tJTxYcd9s6B4qlF8rwIzJQUz44j/sMTwowWyZc37LUK/dI9u38Y5Kgd350v+8YD/clo57tN/Lwz/coZ15Um46wK2pse/9c+/MA96KMs9MEX5bo31ZwQ970S/dFr+8ATuIAX989T99Bd/s9a36M+/soQ8MQ676kxyI0T+8Nn4bpf/sgX1ZUh+7A7/8wA////yg4fqwAAAIB0Uk5T/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////wA4BUtnAAALxElEQVR42rSbiSMb6xqHp5OEZIIgCVH7llhuuCFEpJZEbA1xCKoVS1HcHj2c29Zpq6P/+v2+Wb91FnV//8DzzPu+3zIThF+8xBoTHR0dTU1N4XBPT8/KSl1dfX293+8PBoMNDQ1XV4FAwOsNgbSANDc3x+PxwcGhob6+zs7OaPQRJhptVeLxS1yMwMcnnk/A4/HGXApICUwA8m0FBi0EZs4bXQnA5/9tgSgqMHMYcyEQkzSBJo7AFRTwGgJ6B6wEFjpcCDRiHQijI+DXCuBeIO1CQOILBJ8ssBBzIUCMgJUAvgisBBpdCnTwR0CfQS97FRICHvcCCXYHnG4DnfoqfG4BP7YI/t8CSAcczCC2DbQ+o0CYMwIOFsFvCrALYD2D/FXoXqCDXQBkG2KOgNUqdCXQwSgAtwM2M6gLtLkUaGIXgOgAawQ4M+hGAOVzRtB2BH5TQONzCsDrwPMLUBPA6IDNRqyOgK1ADAu7AUQHQpwOtHJmsE1qRIMKxNQLkL4BK3iDzy9AulDIwuTzH510oK1tdvbNm97eycnJmpoaQRgTNQEST/GZBXgVeWlmaekgrxTAmYDCF4aHxxoVAQnHM/lYAeAI3mN4kFyQvwsgAr2oQNdZIxBoxOgqXuP3oHy8AAWCv7QkBZgdMPZBlkDX2C8h1oHGwPP4+hosEPilWslv1wFVYBIV6BeFRpJu8FdIProEpkh+bdLPW4TsGVQF1gSJoCt4nI8XQF0CWQJfW7tZz+8AZwSAQL9gkjE8l48KIHggEEYKwNgF2AIDAkVn8/882DpET4E9/PFBymFkDbzb29ulR4DuACoQxvE4fws8r+9e5SsCB/jjgyyHzQ58yFWr1Q8ORgAIhLH0IHiTv+FTOl57b54CByS/dlkyOnBTVXJzYrMISYEeA4/z7+/0NVfQN+GpHIHP5d6WtXPgRONXq99ObBZh/4Am0GOExvvva81NZwrw0wUf9fg5mMjNLijAyTedH4lsnWNrgCnQ00PQFTyH//Ll1uFULdX+nJG93d1vBh5k64HTAUpgZQWhq3gWH1n9tQx+tVpF8dDg3GoR9g/8S1hBUkfh9fmj8EuO8CCrLZYdMATq6jC6hg8q688tH8H7fL7Rc0YBEIE6NPX1BH/Khm+LB3n/wC6A0gFZoOEaHe6+9zbtd4D3+Ur/teiALNST8Zt8qgB2fJoOs7nB7wAp4EfxwYY/7fhvQXw0H8X7SskmfgcQAT+SYFA7/rf4/K3lzYSkZvPHKhfv296UetAC4B2QBT+VoIEHecXm340mJSmxEvS2gJvgYOiqPvyjxORvb5elpge1AMgmwBcIYvirq0Mfi78Mnn0lpL4LaFfReMP7FyR+G2RZSqTZHVD4qEAwiNPh6Q8M7ij+W/D09S1xjA9PwXSFwm+PShLk80YQCASpoHhw+ql7McJflqRwKG7ykYtQoYTjt1cT0iF3E2AJNDSgeOX6ESjg/LIkBeNs/uPj7jaK395KSPVtWAGIEUQFGpAgeHD83hF8b3PcfBnE+NGoYrCtZ0SqY21CmEADlSsMr9zAeXxtAFrNmyg0MPigAg/sAmibAC1whdIVvNd7h/BHWXykAOAqvGvyS6Vy2qYAiMCVGRQfKiD8LUkKxLkDoF3Fswa+VFoNcwugC1wRCQQwfCi0hwxAUqq35CsCJy90PEgyTRQAH0FCIBCg8KH0EtqADnt+a+s7A18qLdchmyCjALIQoOM18aHQEbIBJpUGDBJ8vAHwXWDf4L+YT1gXgBLQ4N77wlE2++rg4ADhj0pNFL+VLoDH807HK/l5eZk9OjpiFwAR8CIJTWEngP7yBQtgywcCCy9MPMw+zE+BVQBZ8FKBhT9gnEBVKeGM7/FkKTxI5ppRAEIgpKeWUYBVqU7D2/Fn8iUKv7+fLJJ7gCkQIsPg535IIWd8z8wtjd8f33nNKIAshBhpSTP4uU0JK7/JfyT5MzM0fnx8JMkoAEMAfnssUALg/pdoQh/fXH9UA8C70CWGH1cykmEUABNoMVJgFCAnrTjmQwECDwREBl8WWlgpMPg5ya/gbfjqy+gliZ+YmFAE+h0JNOfpBigCOt7Yfzn8tksKDwUYBaAEmpXkGQXI6R8CifGjBhC+i15SeFVA4XMEmpHkGfxc0m88vh1fFcDw6+uGgIwJNDMS/0g3ACzDemd8KEDjFQGaTwso522cUYDqjzCOR9tP8D9pfAS/vr4o9lsKxNEwClBd7XDKb8ujj6/iLy52Mgy+LMQZGRw8oPlVnxQn8Dz+bJbGX1xkkgw+JTCoZo8UgC+/5YBD/q3BN/Hz86K1wCCaod2DO7IA1erqyqMj/mwee3yI/1Kp7IinDL4sDJIZUtMkScnNzfLyqvkFIpKMM/AkHwj8NB//eGdnR1ST7GbwMYEhM2DLGQSnA3hj/dv8AhJZDTri55Hyj4ivi8WxublrOIBMgSEqfVrUVd/5zShAJLLZR+IZ/Nt1g39xkRTPtEO4n8knBPr6CDrIB5Mf2fJHGY+P898cId0/FtVLQBePLwt9VDqRgF3/JGIKREZb6Mcn+Hl0+hbFosl3INDZSdDhws8iH6B87/sY5Uf5n9aN8l/MpzKZM53PLoAsdHKi0UFOIoYA/OzIfHw2fx6MoMrnNYAjoML1TWc3YhYApMAvP8mfz4hnw9Z8SkBjGzseKMABWgAQ+EOIicf4+QmMP/+laD0AugB8oKiZRzTq7w/oB7jtvV02/zZr8ufVVK5t+LIQxfJI5aZKFAC+e2dv6fbfHq1T/Pn55TOLAVQEHi0TpfjaB5jsLvr4bbOfjibGaX4qlRoZsORbCcCKfMixCqB8fxjP5j+p/E/5o0vt+KP4qVS7JZ8roHbkXa5KTYDx+aWkvnvr108OP3X83YrPEkBGYo9TAOTt37j+agIXhkBKS2XNoUCUCFgaqoBFARzwU8tiu5VAlBlts8E3QR6f2QCDf5xxKdCK5p0TAa0AbH5qUUzKTgRa2fngmD/B5o+IGdlSoNUmN04EyAaY/Ipo2QA7Abjf3WACq8nRbYsCHJePMYFKRrRcA1YCHj03CH9Ukur+Ht1nF+Cisgiunsc4f0e2FRjis9Uj58YQWJbg7w/ndeUKPYFfykmAHxvuOnbDhwItPLJ+4M1kNX5ZShwqB8/DPV0AUcz8da3cv/6jbwAO+LLg8YQ8nMxoWVjIw58hSpvw9yd47oKDn16C4l+Cfv/8R5t/cVp2IuCdsYArB+7Cwsd9Xymp/v4Fv36/6aUKkNKun8r955/j1PGOmFmzo3d3y8K5p6WFw9bgyon30CMpv/+p+F5dwCxARSwa/IEBeTEjZtptH38N/mDh+drwFeeicO28B0mnHww8FCCWQEWc6zLuv+D8m3ZQ/m6gKEgzM6GNcxYagaudh3QVPzn5ExVQliAU6Df4TtINHNeEGIAfbqS/IlwSrtJR/OQkeQ24GAEC/TbXLzzt092yLAq/OgDMu7FxSIZDV/BMAcvH714j8n0a8qfhHzSugOc9P9wgQ9BRfE1NzSXRgYuyaFn+9mkia3BE12LK35SG26ikNzZmZ9l05cefGuEyha7BClj0luVvZw3ldEz/o9aVdBve8/QffxhwjY7i4a8vr8Xkjpqk8v1hzXL6aIHuRZH/d8VQAINrdBMPcz1XVHJ6OjY3YDP9UMDFHza/+TcU6GXTNTzceJWtx9Hie4IADjfoHLzd4ncp0KsJ8OgY3tHe8wSBSS7dPd61wKQuUFPDfHgM72jrdS3w+TMKxx/ePd61QA0QoOEq/Sn4pwsITLpr/NMEBA79CXjXAsL1588WdNd49wJnnz9fI3CK7hIPzr3p7+4EisWx4WEG/Wl4cPmZXnMjMDw8VixeI3Cc7hYPCwCuHy4EisPX4JS7fia6wv8uyy7+5TPZ1QVKUJzr1+Eq/Yn4bnD9AgVod/NvvwA8dnoKD3ol7b8TiFfun27+8TkDHnru1Mj0bwa+Iq25+9fvHVj27vbniazeP10J/MroTX9S25n3T3cCv2LJtWeCy+07jVzM/wQYAFew4WbyXXL+AAAAAElFTkSuQmCC',
          ),
        ),
      ),
    ),
  ),
);

?>