<?php
/**
 * Déclarations relatives à la base de données
 *
 * @plugin     Zora Api
 * @copyright  2013
 * @author     Matthieu Marcillaud
 * @licence    GNU/GPL
 * @package    SPIP\Zoraapi\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Déclaration des alias de tables et filtres automatiques de champs
 *
 * @pipeline declarer_tables_interfaces
 * @param array $interfaces
 *     Déclarations d'interface pour le compilateur
 * @return array
 *     Déclarations d'interface pour le compilateur
 */
function zoraapi_declarer_tables_interfaces($interfaces) {

	$interfaces['table_des_tables']['api_fonctions'] = 'api_fonctions';
	
	$interfaces['table_des_traitements']['DEFINITION']['api_fonctions'] = _TRAITEMENT_RACCOURCIS;
	$interfaces['table_des_traitements']['COMPATIBILITE']['api_fonctions'] = _TRAITEMENT_TYPO;
	$interfaces['table_des_traitements']['DESCRIPTION']['api_fonctions'] = _TRAITEMENT_RACCOURCIS;
	$interfaces['table_des_traitements']['PARAMETRES']['api_fonctions'] = _TRAITEMENT_RACCOURCIS;
	$interfaces['table_des_traitements']['RETOUR']['api_fonctions'] = _TRAITEMENT_RACCOURCIS;
	$interfaces['table_des_traitements']['HISTORIQUE']['api_fonctions'] = _TRAITEMENT_RACCOURCIS;
	$interfaces['table_des_traitements']['EXEMPLES']['api_fonctions'] = _TRAITEMENT_RACCOURCIS;
	$interfaces['table_des_traitements']['NOTA']['api_fonctions'] = _TRAITEMENT_RACCOURCIS;
	$interfaces['table_des_traitements']['VOIR_AUSSI']['api_fonctions'] = _TRAITEMENT_RACCOURCIS;
	
	return $interfaces;
}


/**
 * Déclaration des objets éditoriaux
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function zoraapi_declarer_tables_objets_sql($tables) {

	$tables['spip_api_fonctions'] = array(
		'type' => 'api_fonction',
		'principale' => "oui", 
		'table_objet_surnoms' => array('apifonction'), // table_objet('api_fonction') => 'api_fonctions' 
		'field'=> array(
			"id_api_fonction"    => "bigint(21) NOT NULL",
			"id_rubrique"        => "bigint(21) NOT NULL DEFAULT 0", 
			"id_secteur"         => "bigint(21) NOT NULL DEFAULT 0", 
			"nom"                => "tinytext NOT NULL DEFAULT ''",
			"definition"         => "tinytext NOT NULL DEFAULT ''",
			"compatibilite"      => "tinytext NOT NULL DEFAULT ''",
			"nom_complet"        => "tinytext NOT NULL DEFAULT ''",
			"fichier"            => "tinytext NOT NULL DEFAULT ''",
			"description"        => "mediumtext NOT NULL DEFAULT ''",
			"parametres"         => "mediumtext NOT NULL DEFAULT ''",
			"retour"             => "mediumtext NOT NULL DEFAULT ''",
			"historique"         => "mediumtext NOT NULL DEFAULT ''",
			"exemples"           => "text NOT NULL DEFAULT ''",
			"nota"               => "mediumtext NOT NULL DEFAULT ''",
			"voir_aussi"         => "mediumtext NOT NULL DEFAULT ''",
			"date_publication"   => "datetime NOT NULL DEFAULT '0000-00-00 00:00:00'", 
			"date_modif"         => "datetime NOT NULL DEFAULT '0000-00-00 00:00:00'", 
			"statut"             => "varchar(20)  DEFAULT '0' NOT NULL", 
			"lang"               => "VARCHAR(10) NOT NULL DEFAULT ''",
			"langue_choisie"     => "VARCHAR(3) DEFAULT 'non'", 
			"id_trad"            => "bigint(21) NOT NULL DEFAULT 0", 
			"maj"                => "TIMESTAMP"
		),
		'key' => array(
			"PRIMARY KEY"        => "id_api_fonction",
			"KEY id_rubrique"    => "id_rubrique", 
			"KEY id_secteur"     => "id_secteur", 
			"KEY lang"           => "lang", 
			"KEY id_trad"        => "id_trad", 
			"KEY statut"         => "statut", 
		),
		'titre' => "nom AS titre, lang AS lang",
		'date' => "date_publication",
		'champs_editables'  => array('nom', 'definition', 'compatibilite', 'nom_complet', 'fichier', 'description', 'parametres', 'retour', 'historique', 'exemples', 'nota', 'voir_aussi'),
		'champs_versionnes' => array('nom', 'definition', 'compatibilite', 'nom_complet', 'fichier', 'description', 'parametres', 'retour', 'historique', 'exemples', 'nota', 'voir_aussi'),
		'rechercher_champs' => array("nom" => 7, "definition" => 3),
		'tables_jointures'  => array(),
		'statut_textes_instituer' => array(
			'prepa'    => 'texte_statut_en_cours_redaction',
			'prop'     => 'texte_statut_propose_evaluation',
			'publie'   => 'texte_statut_publie',
			'refuse'   => 'texte_statut_refuse',
			'poubelle' => 'texte_statut_poubelle',
		),
		'statut'=> array(
			array(
				'champ'     => 'statut',
				'publie'    => 'publie',
				'previsu'   => 'publie,prop,prepa',
				'post_date' => 'date', 
				'exception' => array('statut','tout')
			)
		),
		'texte_changer_statut' => 'api_fonction:texte_changer_statut_api_fonction', 
		

	);

	return $tables;
}



?>
