<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// Z
	'zoracode_description' => 'Intègre des composants Zend Framework 2 (autoload) 
	    et [PHP-Token-Reflection->https://github.com/Andrewsville/PHP-Token-Reflection].
	    Fournit une lib pour utiliser cela.',
	'zoracode_slogan' => 'Extraction et analyse de données sur du code PHP',
);
?>
