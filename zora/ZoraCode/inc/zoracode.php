<?php

/**
 * Librairie pour analyser un code source PHP
 *
 * @package SPIP\Zoracode\Fonctions
**/

use \TokenReflection\Broker;

/**
 * Retourne un docblock
 *
 * @param array $options
 *     - file : chemin du fichier
 *     - function : nom de la fonction
 * @return array
**/
function getDocBlock($options = array()) {
	$options += array('file' => '', 'element' => '', 'type_element' => '');
 
	$file         = $options['file'];
	$element      = $options['element'];
	$type_element = $options['type_element'];

	// on ne peut rien calculer sans fichier
	if (!$file) $type_element = '';

	switch ($type_element) {
		case 'file':
			$docblock = getDocBlockFile($file);
			break;
		case 'function':
			$docblock = getDocBlockFunction($element, $file);
			break;
		default:
			$docblock = array('docblock' => '', 'erreur'=>'');
			break;
	}

	return $docblock;
}

/**
 * Retourne le docblock d'un fichier
 *
 * @param string $file Chemin du fichier
 * @return array Infos du Docblock
 *     - docblock : Dockblock, sinon false
 *     - annotation : Docblock compilé, sinon false
 *     - start : Numéro de ligne du commencement du docblock
 *     - end : Numéro de ligne de fin du docblock
 *     - source : Code Source
**/
function getDocBlockFile($file) {
	// $file = "/home/marcimat/www/spip-dev/ecrire/inc/filtres.php";
	$broker = new Broker(new Broker\Backend\Memory());

	try {
		$f = $broker->processFile($file, true);

		$start = $end = $addspace = null;

		// pas de docblock ? chercher la position où mettre ce nouveau doc dans le code
		if (!$docblock = $f->getDocComment()) {
			// on prend après le premier commentaire non doc s'il existe
			// avant autre chose qu'un commentaire doc sinon.
			$tokens = $broker->getFileTokens($file);
			$tokens->rewind();
			$tokens->find(T_OPEN_TAG);
			$tokens->next()->next(); // on est propre, on a au moins un saut de ligne !

			// y a des chances que ce soit l'entête SPIP
			// on considère le prochain espace comme approprié pour le docblock
			if (($tokens->getType() == T_COMMENT)
			and substr($tokens->getTokenValue(), 1, 10) == '**********') {
				$tokens->next()->next();
			} 
			// sinon on considère le blanc comme approprié pour le docblock

			$t = $tokens->current();
			$start = $t[2] - 1;
			$end = $start;
			$addspace = true;

		} 

		return array(
			'docblock' => $docblock,
			'annotation' => $f->getAnnotations(),
			'start' => $start,
			'end' => $end,
			'source' => $f->getSource(),
			'addspace' => $addspace,
			'erreur' => '',
		);
	}
	catch (Exception $e) {
		return array(
			'docblock' => null,
			'annotation' => null,
			'start' => null,
			'end' => null,
			'source' => null,
			'erreur' => $e->getMessage(),
		);
	}
}


/**
 * Retourne le docblock d'une fonction d'un fichier
 *
 * @param string $function Nom de la fonction
 * @param string $file Chemin du fichier
 * @return array Infos du Docblock
 *     - docblock : Dockblock, sinon false
 *     - annotation : Docblock compilé, sinon false
 *     - start : Numéro de ligne du commencement du docblock
 *     - end : Numéro de ligne de fin du docblock
 *     - source : Code Source
**/
function getDocBlockFunction($function, $file) {
	// $file = "/home/marcimat/www/spip-dev/ecrire/inc/filtres.php";
	$broker = new Broker(new Broker\Backend\Memory());
	try {
		$broker->processFile($file);
		$f  = $broker->getFunction($function);

		// pas de docblock ? chercher les commentaires old school avant...
		if (!$docblock = $f->getDocComment()) {
			$tokens = $broker->getFileTokens($file);
			$tokens->rewind();
			while ($tokens->find(T_FUNCTION)) {
				$i = $tokens->key();
				$tokens->skipWhitespaces(true);
				if ($tokens->getTokenValue() == $function) {
					$comments = array();
					while (--$i and ($tokens->getType($i) == T_COMMENT)) {
						$comments[] = $tokens->getTokenValue($i);
					}
					if ($comments) {
						$comments = array_reverse($comments);
						$comments = implode('', $comments);
						$docblock = trim($comments);
					}
					break;
				} else {
					$tokens->next();
				}
			}
		}
		return array(
			'docblock' => $docblock,
			'annotation' => $f->getAnnotations(),
			'start' => $f->getStartLine() - 1 - substr_count($docblock, "\n"),
			'end' => $f->getStartLine() - 1,
			'source' => $f->getSource(),
			'erreur' => '',
		);
	}
	catch (Exception $e) {
		return array(
			'docblock' => null,
			'annotation' => null,
			'start' => null,
			'end' => null,
			'source' => null,
			'erreur' => $e->getMessage(),
		);
	}
}

