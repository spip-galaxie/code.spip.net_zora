<?php

if (!defined('ZORACODE_SOURCE')) {
	define('ZORACODE_SOURCE', _DIR_RACINE . '../code_source/');
}

/*
Doc

Procédure : télécharger ZF2 minimal http://framework.zend.com/downloads/latest
Copier librairy sous un autre nom (lib) et tout virer Zend/* sauf
- Code
- EventManager
- StdLib

Et il faut garder pour l'autoloader et sa génération :
- Loader
- Console
- File

Puis en terminal :

cd bin
php classmap_generator.php -l ../lib/Zend/

Cela donne l'autoloader.
Copier lib dans ce plugin… Et voilou :)

Suivre bin/autoload_exemple pour le chargement
*/
require_once __DIR__ . '/lib/Zend/Loader/ClassMapAutoloader.php';
#global $loader;
$loader = new \Zend\Loader\ClassMapAutoloader();
$loader->registerAutoloadMap(__DIR__ . '/lib/autoload_classmap.php');
$loader->register();

