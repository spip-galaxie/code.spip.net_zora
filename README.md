Plugins & squelettes pour code.spip.net


## Installation

- cloner le dépot
- exécuter checkout.sh (necessite checkout.php)
- lier plugins/ à /plugins du SPIP
- activer les plugins...

## Pour l’autodoc

### Compléter le htaccess

	```
	# REGLAGES PERSONNALISES
	RewriteRule ^@(.+)$     autodoc/@$1 [QSA,R,L]
	```


