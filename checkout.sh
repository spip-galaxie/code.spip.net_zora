# checkout.php git -bmaster {...}/antispam.git plugins/antispam

checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/bootstrap2.git plugins/bootstrap2
checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/bootstrap4.git plugins/bootstrap4
checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/boussole.git plugins/boussole
checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/champs_extras_core.git plugins/champs_extras_core
checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/coloration_code.git plugins/coloration_code
checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/comments.git plugins/comments
checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/crayons.git plugins/crayons
checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/facteur.git plugins/facteur
checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/less-css.git plugins/less-css
checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/menus.git plugins/menus
checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/nospam.git plugins/nospam
checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/notifications.git plugins/notifications
checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/opensearch.git plugins/opensearch
checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/oembed.git plugins/oembed
checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/porte_plume_codes.git plugins/porte_plume_codes
checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/porte_plume_codes_spip.git plugins/porte_plume_codes_spip
checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/pages.git plugins/pages
checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/precode.git plugins/precode
checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/saisies.git plugins/saisies
checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/scssphp.git plugins/scssphp
checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/todo.git plugins/todo
checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/spip-bonux.git plugins/spip-bonux
checkout.php git -bmaster https://git.spip.net/spip-contrib-extensions/z-core.git plugins/z-core

checkout.php git -bmaster https://git.spip.net/spip-contrib-squelettes/spipr-dist.git plugins/spipr-dist
checkout.php git -bv0 https://git.spip.net/spip-contrib-squelettes/spipr-dist.git plugins/spipr-dist_v0

if [ ! -L plugins/zora ]; then
    cd plugins 
    ln -s ../zora .
    cd ..
fi